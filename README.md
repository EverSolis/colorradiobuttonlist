# ColorRadioButtonList

## OverView

![alt text](https://github.com/EverK777/Images/blob/master/color_list.png?raw=true) 


---

## Configuration
### Add Module to the app
1. Download the repository
2. Import the library as module
3. Add the module to the gradle app dependecies ```implementation project(':ColorRadioButton)```


### Add ColorRadioButtonList to your xml

```
      <com.kadevjo.colorradiobutton.ColorRadioButtonGroup
        android:id="@+id/colorRadioGroup"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:clickAnimationEnable="true"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
```
			
### Custom properties

* ```  app:clickAnimationEnable="true" ``` Enable animation when an item is clicked
* ```        app:insideCircleVisible="false" ``` Show the circle inside of the check


### Set data

```
   val colors : ArrayList<String> = ArrayList()
        colors.add("#FED1D0")
        colors.add("#DAFCCC")
        colors.add("#D0FCED")
        colors.add("#CAC7E1")
        colors.add("#F5F2FF")
        colors.add("#3F51B5")
        colors.add("#03A9F4")
        colors.add("#6eabce")
        colorRadioGroup.setColors(colors)

```


### Listener of item click
```
      colorRadioGroup.setOnItemClick { color, isChecked, recyclerPosition ->
            Snackbar.make(mainView, "The color selected is $color", Snackbar.LENGTH_LONG)
                .show()
        }
```

