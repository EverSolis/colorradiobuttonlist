package com.kadevjo.colorradiobutton.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import android.widget.RelativeLayout
import com.kadevjo.colorradiobutton.interfaces.OnCheckedChangeListener

/**
 * View that simulates a checkbox but like a Checkable container
 */

class CheckableViewGroup : RelativeLayout, Checkable {

    private var checkedState = false
    private var enableState = true
    private val checkedStateSet = intArrayOf(android.R.attr.state_checked)
    private val enableStateSet = intArrayOf(android.R.attr.state_enabled)
    private lateinit var onCheckedChangeListener: OnCheckedChangeListener

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        this.setOnClickListener {
            if (isEnabled) {
                if(!isChecked){
                    isChecked = true
                }
            }
        }

    }


    override fun isChecked(): Boolean {
        return checkedState
    }

    override fun toggle() {
        isChecked = checkedState
    }

    override fun isEnabled(): Boolean {
        return enableState
    }

    override fun setEnabled(state: Boolean) {
            enableState = state
            if (!enableState) {
                this.alpha = 0.7f
                for (i in 0 until this.childCount) {
                    this.getChildAt(i).alpha = 0.7f
                }
                refreshDrawableState()
            }else{
                this.alpha = 1f
                for (i in 0 until this.childCount) {
                    this.getChildAt(i).alpha = 1f
                }
                refreshDrawableState()
            }
    }

    override fun setChecked(state: Boolean) {
        if (state != checkedState) {
            checkedState = state
            refreshDrawableState()
            if (::onCheckedChangeListener.isInitialized) {
                onCheckedChangeListener.onCheckedChanged(this, checkedState)
            }
        }
    }

    /**
     * function that triggers the checked state
     */
    fun setOnCheckedChangeListener(onClick: (checkableView: View, isChecked: Boolean) -> Unit) {
        onCheckedChangeListener = object  : OnCheckedChangeListener{
            override fun onCheckedChanged(checkableView: View, isChecked: Boolean) {
                onClick.invoke(checkableView,isChecked)
            }

        }
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 2)
        if (isChecked) {
            View.mergeDrawableStates(drawableState, checkedStateSet)
        }
        if (isEnabled) {
            View.mergeDrawableStates(drawableState, enableStateSet)
        }
        return drawableState
    }

}

