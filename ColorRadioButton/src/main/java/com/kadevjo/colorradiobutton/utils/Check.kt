package com.kadevjo.colorradiobutton.utils

/**
 * Class that helps to populate the recyclerview color
 * @param color the color in Hexadecimal
 * @param isChecked if the colorCheckBox state
 */
data class Check(
    var color : String,
    var isChecked : Boolean
)