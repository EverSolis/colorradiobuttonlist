package com.kadevjo.colorradiobutton.interfaces

import android.view.View

interface OnCheckedChangeListener {
    /**
     *  function that listen when the check state change
     *  @param checkableView the main view
     *  @param isChecked status of the checked
     */
    fun onCheckedChanged(checkableView: View, isChecked: Boolean)
}