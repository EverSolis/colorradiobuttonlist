package com.kadevjo.colorradiobutton.interfaces


interface OnItemCheckListener {
    /**
     * Is triggered when a ship is clicked
     * @param color the Color in Hexadecimal
     * @param isChecked if the item is checked
     * @param recyclerPosition the position of the RecyclerView Adapter
     */
    fun onCheck (color : String ,isChecked : Boolean, recyclerPosition : Int)
}