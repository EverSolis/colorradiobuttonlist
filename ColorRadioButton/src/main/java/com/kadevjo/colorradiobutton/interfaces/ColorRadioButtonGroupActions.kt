package com.kadevjo.colorradiobutton.interfaces

interface ColorRadioButtonGroupActions {
    /**
     * Set the colors list to populate the RecyclerView
     */
    fun setColors (colors : List<String>)
    /**
     * Validate it the view has the animation when click
     */
    fun setClickAnimation(isActive: Boolean)
    fun setOpenAnimation(isActive: Boolean)
    /**
     * If the view has a inside circle view of it
     */
    fun insideCircleVisible(isActive: Boolean)
}