package com.kadevjo.colorradiobutton

/**
 * @author Ever Antonio Morale Solis
 */
import android.animation.Animator
import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.kadevjo.colorradiobutton.interfaces.ColorRadioButtonGroupActions
import com.kadevjo.colorradiobutton.interfaces.OnItemCheckListener
import com.kadevjo.colorradiobutton.utils.Check
import com.kadevjo.colorradiobutton.utils.DynamicAdapter
import kotlinx.android.synthetic.main.color_radio_button_group.view.*
import kotlinx.android.synthetic.main.item_check.view.*
import java.lang.Exception

/**
 * Custom view that creates a list of colors that each color returns a color in Hexadecimal
 * @property checkedAnimationEnable when is true the color applies an animation when is clicked
 * @property insideCircleVisible if the color has a circle inside of it
 * @property adapter the adapter of the RecyclerView
 * @property data the list of the colors
 * @property listener interface that listen when a color is checked
 */

class ColorRadioButtonGroup : RelativeLayout, ColorRadioButtonGroupActions {

    private var openAnimationEnable = true
    private var checkedAnimationEnable = true
    private var insideCircleVisible = true
    private var adapter: DynamicAdapter<Check>? = null
    private val data: ArrayList<Check> = ArrayList()
    private var listener : OnItemCheckListener ?= null


    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
    ) : super(context, attrs, defStyleAttr) {
        config(attrs)

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int,
    ) : super(context, attrs, defStyleAttr, defStyleRes) {

        config(attrs)
    }

    private fun config(attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.color_radio_button_group, this, true)
        attrs.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.ColorRadioButtonGroup,
                0,
                0
            )

            this.openAnimationEnable =
                typedArray.getBoolean(R.styleable.ColorRadioButtonGroup_openAnimation, true)
            this.checkedAnimationEnable =
                typedArray.getBoolean(R.styleable.ColorRadioButtonGroup_clickAnimationEnable, true)
            this.insideCircleVisible =
                typedArray.getBoolean(R.styleable.ColorRadioButtonGroup_insideCircleVisible, true)

            configRecyclerView()

            typedArray.recycle()
        }
    }

    private fun configRecyclerView() {
        adapter = DynamicAdapter(
            layout = R.layout.item_check,
            entries = data,
            action = fun(_, view, item, position) {

                view.mainView.background.setTint(getColorFromHexadecimal(item.color))

                view.mainView.setOnCheckedChangeListener { _, isChecked ->

                    if(listener != null)
                        listener?.onCheck(item.color,isChecked,position)

                    if (isChecked) {
                        item.isChecked = true


                        if (checkedAnimationEnable) {
                            //apply animation circular
                            if (insideCircleVisible)
                                view.progressBar.makeProgressAnimation
                            //apply check animation
                            view.imageCheck.applyShapeAnimation
                            view.imageCheck.applySpringAnimation
                        } else {
                            if (insideCircleVisible){
                                view.progressBar.makeVisibleWithProgress
                            }
                            view.imageCheck.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_baseline_check_24))
                            view.imageCheck.visibility = View.VISIBLE
                        }

                        for (i in data.indices) {
                            if (i != position){
                                data[i].isChecked = false
                                view.mainView.isChecked = false
                                adapter?.notifyItemChanged(i)
                            }

                        }
                    }
                }

                if(item.isChecked){
                    view.imageCheck.visibility = View.VISIBLE
                    if (insideCircleVisible){
                        view.progressBar.visibility = View.VISIBLE
                        view.progressBar.progress = 100
                    }
                }else{
                    view.imageCheck.visibility = View.INVISIBLE
                    if (insideCircleVisible){
                        view.progressBar.visibility = View.INVISIBLE
                        view.progressBar.progress = 0
                    }
                }
            }
        )

        recyclerColors.adapter = adapter
        recyclerColors.itemAnimator = null
    }

    private fun getColorFromHexadecimal(color: String): Int {
        return try {
            Color.parseColor(color)
        }catch (e : Exception){
            Color.parseColor("#FF03DAC5")
        }
    }

    override fun setColors(colors: List<String>) {
        data.clear()
        colors.forEach {
            data.add(Check(it, false))
        }
        if (this.adapter != null)
            adapter?.notifyDataSetChanged()
    }



    override fun setClickAnimation(isActive: Boolean) {
        this.checkedAnimationEnable = isActive
    }

    override fun setOpenAnimation(isActive: Boolean) {
        this.openAnimationEnable = isActive
    }

    override fun insideCircleVisible(isActive: Boolean) {
        this.insideCircleVisible = isActive
    }


    fun setOnItemClick (onClick: (color : String ,isChecked : Boolean, recyclerPosition : Int) -> Unit){
        listener = object : OnItemCheckListener{
            override fun onCheck(color: String, isChecked: Boolean, recyclerPosition: Int) {
                onClick.invoke(color,isChecked,recyclerPosition)
            }

        }
    }

    private val ProgressBar.makeVisibleWithProgress: Unit
        get() {
            this.progress = 100
            this.visibility = View.VISIBLE
        }


    private val ImageView.applySpringAnimation: Unit
        get() {


            val  springAnimationY =  this.let { img ->
                SpringAnimation(img, DynamicAnimation.SCALE_Y, img.scaleY)
            }
            val  springAnimationX = this.let { img ->
                SpringAnimation(img, DynamicAnimation.SCALE_X, img.scaleX)
            }

            springAnimationX.setMaxValue(this.scaleX + (this.scaleX/2))
            springAnimationY.setMaxValue(this.scaleY + (this.scaleY/2))

            val velocity = 500f
            springAnimationY.setStartVelocity(velocity)
            springAnimationY.spring.apply {
                dampingRatio  = SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY
                stiffness     = SpringForce.STIFFNESS_MEDIUM
                dampingRatio  = SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY
                stiffness     = SpringForce.STIFFNESS_MEDIUM
            }


            this.scaleX = 0f
            this.scaleY = 0f


            springAnimationY.start()
            springAnimationX.start()


        }

    private val ImageView.applyShapeAnimation: Unit
        get() {
            this.visibility = View.VISIBLE
            val drawable = this.drawable

            if(drawable is AnimatedVectorDrawableCompat){
                drawable.start()
            }else if(drawable is AnimatedVectorDrawable){
                drawable.start()
            }
        }


    private val ProgressBar.makeProgressAnimation: Unit
        get() {

            this.visibility = View.VISIBLE
            val animation = ObjectAnimator.ofInt(this, "progress", 100)
            animation.duration = 150
            animation.interpolator = AccelerateDecelerateInterpolator()
            animation.start()
        }

    private fun ObjectAnimator.onEndAnimation(endAnimation: () -> Unit) {
        this.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                endAnimation.invoke()
            }

            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationRepeat(animation: Animator?) {}
        })
    }
}




