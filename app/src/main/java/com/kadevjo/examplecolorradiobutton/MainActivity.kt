package com.kadevjo.examplecolorradiobutton

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val colors : ArrayList<String> = ArrayList()
        colors.add("#FED1D0")
        colors.add("#DAFCCC")
        colors.add("#D0FCED")
        colors.add("#CAC7E1")
        colors.add("#F5F2FF")
        colors.add("#3F51B5")
        colors.add("#03A9F4")
        colors.add("#6eabce")
        colorRadioGroup.setColors(colors)

        colorRadioGroup.setOnItemClick { color, isChecked, recyclerPosition ->
            Snackbar.make(mainView, "The color selected is $color", Snackbar.LENGTH_LONG)
                .show()
        }

    }
}