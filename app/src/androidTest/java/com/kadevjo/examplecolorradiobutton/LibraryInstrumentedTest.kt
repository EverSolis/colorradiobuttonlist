package com.kadevjo.examplecolorradiobutton

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.kadevjo.colorradiobutton.utils.DynamicAdapter
import com.kadevjo.examplecolorradiobutton.helpers.RecyclerViewItemCountAssertion
import com.kadevjo.examplecolorradiobutton.helpers.RecyclerViewMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * 1. RecyclerView is displayed
 * 2. RecyclerView is not empty
 * 3. Recycler item click return data expected
 * 4. imageView is visible when is checked
 * 5. imageView is invisible when is not checked
 */
@RunWith(AndroidJUnit4::class)
class LibraryInstrumentedTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun recyclerViewColors_IsDisplayed(){
        onView(withId(R.id.recyclerColors)).check(matches(isDisplayed()))
    }

    @Test
    fun recyclerViewColors_IsNotEmpty(){
        onView(withId(R.id.recyclerColors)).check(RecyclerViewItemCountAssertion(8))
    }

    @Test
    fun recyclerViewColors_itemClick(){
        onView(withId(R.id.recyclerColors))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<DynamicAdapter.ViewHolder>(0, ViewActions.click()))

        onView(withId(com.google.android.material.R.id.snackbar_text))
                .check(matches(withText("The color selected is #FED1D0")))
    }

    @Test
    fun recyclerViewColors_imageCheckAndProgressAreVisibleWhenChecked(){
        onView(withId(R.id.recyclerColors))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<DynamicAdapter.ViewHolder>(0, ViewActions.click()))

        onView(
            RecyclerViewMatcher(R.id.recyclerColors)
            .atPosition(0,R.id.imageCheck))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        onView(
            RecyclerViewMatcher(R.id.recyclerColors)
            .atPosition(0,R.id.progressBar))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun recyclerViewColors_imageCheckAndProgressAreInvisibleWhenIsNotChecked(){
        onView(withId(R.id.recyclerColors))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<DynamicAdapter.ViewHolder>(0, ViewActions.click()))

        onView(
            RecyclerViewMatcher(R.id.recyclerColors)
            .atPosition(1,R.id.imageCheck))
            .check(matches(withEffectiveVisibility(Visibility.INVISIBLE)))

        onView(
            RecyclerViewMatcher(R.id.recyclerColors)
            .atPosition(1,R.id.progressBar))
            .check(matches(withEffectiveVisibility(Visibility.INVISIBLE)))
    }

}